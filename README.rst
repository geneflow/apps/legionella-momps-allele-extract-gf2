Legionalla mompS Allele Extract GeneFlow App
============================================

Version: 0.2-01

This GeneFlow2 app wraps the custom Legionella mompS allele extract python script.

Inputs
------

1. samfile: Input SAM File

2. input: Input FASTQ File

3. pair: Input FASTQ Pair

Parameters
----------

1. output: Output directory
