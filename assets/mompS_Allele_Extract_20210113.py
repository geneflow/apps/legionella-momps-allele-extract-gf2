#!/usr/bin/python3

import sys
import random
import re
import argparse
from itertools import islice

# find all perfect matches in mompS range in SAM file
def find_perfect_matches(samFile):

    #Corby
    sbtAlLocation = {
        'flaA': [1547139, 1547317],
        'pilE': [759799, 760131],
        'asd': [2717485, 2717957],
        'mip': [975928, 976326],
        'mompS': [3527881, 3528232],
        'proA': [562090, 562494],
        'neuA_neuAH': [931202, 931555]
    }

    # range for mompS
    checkStart = int(sbtAlLocation['mompS'][0])
    checkEnd = int(sbtAlLocation['mompS'][1])

    # list of all matching reads
    mapRead = {}

    # regex for perfect match
    regMatch = re.compile("^([1-9]+)M$")

    try:
        with open(samFile, 'r') as f:
            samItem = f.readline()
            while samItem:

                # skip header info
                if samItem.startswith("@"):
                    samItem = f.readline()
                    continue

                readElement = samItem.split("\t")
                readPos = int(readElement[3])
                quality = readElement[5]
                readElementKey="@"+readElement[0]

                if checkStart <= readPos <= checkEnd:
                    checkMatch = bool(regMatch.match(quality))
                    if checkMatch == True:
                        mapRead[readElementKey] = None

                samItem = f.readline()

    except Exception as err:
        print('ERROR: {}'.format(err))
        return False

    return mapRead


# extract original matching reads from R1 file
def extract_matching_reads(fastqFile, outFile, mapRead):

    try:
        with open(fastqFile, 'r') as f, open(outFile, 'w') as fastqOutput:
            line = f.readline().rstrip()

            while line:
                if line.startswith('@'):
                    key = line.split(' ')[0]
                    # get the next 3 lines
                    fastqLines = list(islice(f, 3))
                    if len(fastqLines) < 3:
                        # probably a truncated file
                        print('WARNING: truncated file')
                        break

                    if key in mapRead:
                        fastqOutput.write(line + "\n")
                        for fastqLine in fastqLines:
                            fastqOutput.write(fastqLine)

                line = f.readline().rstrip()

    except Exception as err:
        print('ERROR: {}'.format(err))
        return False

    return True


# parse command line arguments and return
# as list of argument values
def parse_args():

	parser = argparse.ArgumentParser(
		description='mompS_allele_extract.py'
	)
	parser.add_argument(
		'--sam',
		type=str,
		required=True,
		help='SAM file'
	)
	parser.add_argument(
		'--r1',
		type=str,
		required=True,
		help='R1 fastq file'
	)
	parser.add_argument(
		'--r2',
		type=str,
		required=True,
		help='R2 fastq file'
	)
	parser.add_argument(
		'--outr1',
		type=str,
		required=True,
		help='Output R1 fastq file'
	)
	parser.add_argument(
		'--outr2',
		type=str,
		required=True,
		help='Output R2 fastq file'
	)
	args = parser.parse_args()
	return (
		args.sam,
		args.r1,
        args.r2,
        args.outr1,
        args.outr2
	)


def main():

    (samFile, R1, R2, outR1, outR2) = parse_args()

    mapRead = find_perfect_matches(samFile)
    if mapRead is False:
        print('ERROR: find_perfect_matches() failed')
        sys.exit(1)

    if not extract_matching_reads(R1, outR1, mapRead):
        print('ERROR: extract_matching_reads() failed for {}'.format(R1))
        sys.exit(1)
    if not extract_matching_reads(R2, outR2, mapRead):
        print('ERROR: extract_matching_reads() failed for {}'.format(R1))
        sys.exit(1)


if __name__ == "__main__":
    main()
